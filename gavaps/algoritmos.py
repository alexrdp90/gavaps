# -*- coding: utf-8 -*-

from copy import deepcopy
from random import choice, uniform, shuffle
from itertools import repeat, chain

from gavaps.individuo import Individuo
from gavaps.operadores import muta_individuo, mutacion_inversion, crossover, crossover_ox


def avg(lista):
    return sum(lista, 0.0) / len(lista)


def selecciona(n, lista):
    return [choice(lista) for _ in repeat(None, n)]


def poblacion_inicial_aleatoria(genes, long_individuo):
    """
    Generar la población inicial a partir de genes aleatorios.
    Esto permite tener genes repetidos en un individuo.
    """
    return lambda p: [Individuo([choice(genes) for _ in range(long_individuo)]) for _ in xrange(p)]


def poblacion_inicial_mezcla(l):
    """
    Generar la población inicial a partir de ordenar aleatoriamente la lista.
    De esta forma no tendremos genes repetidos en el individuo.
    """
    def f(p):
        res = []
        for _ in range(p):
            l1 = l[:]
            shuffle(l1)
            res.append(Individuo(l1))
        return res
    return f


class GA(object):
    """
    Representación de un algoritmo genético
    """
    def __init__(self, problema):
        self.genes = problema.genes
        self.long_individuo = problema.long_individuo
        self.decodifica = problema.decodifica
        self.f_objetivo = problema.f_objetivo
        self.op_cruce = crossover
        self.op_mutacion = muta_individuo(self.genes)
        self.poblacion_inicial = poblacion_inicial_aleatoria(self.genes, self.long_individuo)

    def evalua_poblacion(self, g):
        fitness = []
        suma = 0
        for individuo in g:
            f = self.f_objetivo(self.decodifica(individuo))
            fitness.append(f)
            suma += f
            individuo.suma = suma
        self.estadisticas.append([min(fitness), max(fitness), avg(fitness), len(fitness)])
        return suma
    
    def _primero_que_supera(self, s, gev):
        for individuo in gev:
            if individuo.suma > s:
                return deepcopy(individuo)
        return None
        
    def selecciona(self, n, poblacion):
        """
        Método de selección por ruleta.
        """
        return [self._primero_que_supera(uniform(0, self.suma), poblacion) for _ in repeat(None, n)]

    def cruza(self, g):
        return list(chain(*[self.op_cruce(g[i], g[i+1]) for i in xrange(0, len(g), 2)]))

    def muta(self, poblacion, m):
        for individuo in poblacion:
            self.op_mutacion(individuo, m)

    def mejor(self, g):
        mejor = None
        max_val = 0
        for individuo in g:
            val = self.f_objetivo(self.decodifica(individuo))
            if val > max_val:
                mejor = individuo
                max_val = val
        return mejor, self.decodifica(mejor), max_val

    def algoritmo_genetico(self, num_generaciones, tam_poblacion, r, m):
        self.estadisticas = []
        poblacion = self.poblacion_inicial(tam_poblacion)
        num_parejas = int((r * tam_poblacion) // 2)
        individuos_a_cruzar = num_parejas * 2
        individuos_sin_cruzar = tam_poblacion - individuos_a_cruzar
        for _ in range(num_generaciones):
            self.suma = self.evalua_poblacion(poblacion)
            poblacion1 = self.selecciona(individuos_sin_cruzar, poblacion)
            poblacion2 = self.selecciona(individuos_a_cruzar, poblacion)
            poblacion3 = self.cruza(poblacion2)
            poblacion = poblacion1 + poblacion3
            self.muta(poblacion, m)
        return self.mejor(poblacion)


class Allocation(object):
    """
    Clase para la asignación (allocation) de la edad de un individuo.
    
    Los parámetros 'absFitMax' y 'absFitMin' representan los valores máximo y mínimo,
    respectivamente, de la función objetivo de todas las poblaciones.
    """
    def __init__(self):
        self.absFitMin = None
        self.absFitMax = None
    
    def updateAbs(self, minFit, maxFit):
        self.absFitMin = min(self.absFitMin, minFit) if self.absFitMin else minFit
        self.absFitMax = max(self.absFitMax, maxFit) if self.absFitMax else maxFit

    def setMinAndMax(self, minLT, maxLT):
        self.minLT = minLT
        self.maxLT = maxLT
        self.n = (maxLT - minLT) / 2.


class ProportionalAllocation(Allocation):
    def asigna_edad(self, minFit, maxFit, avgFit, fitness):
        return min(self.minLT + self.n * (fitness / avgFit), self.maxLT)


class LinearAllocation(Allocation):
    def asigna_edad(self, minFit, maxFit, avgFit, fitness):
        absFitMin = Allocation.absFitMin
        absFitMax = Allocation.absFitMax
        return self.minLT + 2 * self.n * ((fitness - absFitMin) / (absFitMax - absFitMin))


class BiLinearAllocation(Allocation):
    def asigna_edad(self, minFit, maxFit, avgFit, fitness):
        if avgFit >= fitness:
            return self.minLT + self.n * ((fitness - minFit)/(avgFit - minFit))
        else:
            return (self.minLT + self.maxLT) / 2. + self.n * ((fitness - avgFit)/(maxFit - avgFit))


class GAVaPS(GA):
    """
    Clase para representar un algoritmo genético con tamaño de población variable.
    """
    def __init__(self, problema, allocation = ProportionalAllocation()):
        super(GAVaPS,self).__init__(problema)
        self.allocation = allocation
        self.op_cruce = crossover_ox
        self.op_mutacion = mutacion_inversion
        self.poblacion_inicial = poblacion_inicial_mezcla(problema.genes)
        
    def evalua(self, poblacion, poblacion_aux):
        f = lambda x: self.f_objetivo(self.decodifica(x))
        fitness = map(f, poblacion + poblacion_aux)
        if not len(fitness):
            raise Exception("Población vacía")
        minFit = min(fitness)
        maxFit = max(fitness)
        avgFit = avg(fitness)
        self.allocation.updateAbs(minFit, maxFit)
        for individuo in poblacion_aux:
            edad = self.allocation.asigna_edad(minFit, maxFit, avgFit, f(individuo))
            individuo.edad = int(edad)
        poblacion += poblacion_aux
        self.estadisticas.append([minFit, maxFit, avgFit, len(poblacion)])
        return poblacion

    def algoritmo_genetico(self, num_generaciones, tam_poblacion, r, m, minLT = 1, maxLT = 7):
        self.allocation.setMinAndMax(minLT, maxLT)
        self.estadisticas = []
        poblacion = self.poblacion_inicial(tam_poblacion)
        poblacion = self.evalua([], poblacion)
        for _ in range(num_generaciones):
            map(Individuo.incrementa_edad, poblacion)
            num_parejas = int((len(poblacion) * r) // 2)
            poblacion_aux = selecciona(num_parejas * 2, poblacion)
            poblacion_aux = self.cruza(poblacion_aux)
            self.muta(poblacion_aux, m)
            self.evalua(poblacion, poblacion_aux)
            poblacion = filter(Individuo.vivo, poblacion)
        return self.mejor(poblacion)
