# -*- coding: utf-8 -*-

from random import random, randint, choice

from gavaps.individuo import Individuo


def rango_aleatorio(inicio, fin):
    pos1 = randint(inicio, fin)
    pos2 = randint(inicio, fin)
    return min(pos1, pos2), max(pos1, pos2)


# Crossover clásico
def crossover(individuo1, individuo2):
    pos = randint(1, len(individuo1)-1)
    res1 = individuo1[:pos] + individuo2[pos:]
    res2 = individuo2[:pos] + individuo1[pos:]
    return Individuo(res1), Individuo(res2)


# Partially-mapped crossover
def pmx(p1, p2, inicio, fin):
    def pmx_individuo(p_a, p_b):
        mapa = {p_b[i]: p_a[i] for i in xrange(inicio, fin)}
        off = [mapa[gen] if gen in mapa else gen for gen in p_a]
        off[inicio:fin] = p_b[inicio:fin]
        return Individuo(off)
    return pmx_individuo(p1, p2), pmx_individuo(p2, p1)


def crossover_pmx(individuo1, individuo2):
    pos1, pos2 = rango_aleatorio(0, len(individuo1))
    return pmx(individuo1, individuo2, pos1, pos2)


# Order crossover
def ox(p1, p2, inicio, fin):
    tam = len(p1.genes)
    if inicio == fin:
        return Individuo(p2.genes[:]), Individuo(p1.genes[:])

    def ox_individuo(p_a, p_b):
        off = [None] * tam
        off[inicio:fin] = p_a[inicio:fin]
        i = fin
        j = fin
        while None in off:
            gen = p_b[j%tam]
            if not gen in off:
                off[i%tam] = gen
                i += 1
            j += 1
        return Individuo(off[:])
    
    return ox_individuo(p1, p2), ox_individuo(p2, p1)

def crossover_ox(individuo1, individuo2):
    pos1, pos2 = rango_aleatorio(0, len(individuo1))
    return ox(individuo1, individuo2, pos1, pos2)


# Cycle crossover
def cx(p1, p2):
    def cx_individuo(p_a, p_b):
        off = [None] * tam
        indice = 0
        gen = p_a[indice]
        while not gen in off:
            off[indice] = gen
            indice = p_a.index(p_b[indice])
            gen = p_a[indice]
        for i in xrange(tam):
            if off[i] == None: off[i] = p_b[i]
        return Individuo(off)
    
    tam = len(p1)
    return cx_individuo(p1, p2), cx_individuo(p2, p1)


def crossover_cx(individuo1, individuo2):
    pos1, pos2 = rango_aleatorio(0, len(individuo1))
    return ox(individuo1, individuo2, pos1, pos2)


def inversion(individuo, inicio, fin):
    sublista = individuo[inicio:fin]
    sublista.reverse()
    individuo[inicio:fin] = sublista


def mutacion_inversion(individuo, m):
    if random() < m:
        pos1, pos2 = rango_aleatorio(0, len(individuo))
        inversion(individuo, pos1, pos2)


# Mutación clasica
def muta_individuo(lista_genes):
    return lambda individuo, m: individuo.set_genes([choice(lista_genes) if random() < m else gen for gen in individuo])
