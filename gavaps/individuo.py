# -*- coding: utf-8 -*-

class Individuo(object):
    """
    Clase para representar un individuo: genes,
    suma (para selección por ruleta) y edad (para GAVaPS)
    """
    def __init__(self, genes):
        self.genes = genes
        self.edad = 0
        self.suma = 0
    def __getitem__(self, indice):
        return self.genes[indice]
    def __setitem__(self, indice, valor):
        self.genes[indice] = valor
    def __len__(self):
        return len(self.genes)
    def __str__(self):
        return str(self.genes)
    def __repr__(self):
        return "<Genes: {}, Edad: {}, Suma: {}>".format(self.genes, self.edad, self.suma)
    def index(self, elem):
        return self.genes.index(elem)
    def set_genes(self,genes):
        self.genes = genes
    def vivo(self):
        return self.edad > 0
    def incrementa_edad(self):
        self.edad -= 1
