# -*- coding: utf-8 -*-

import math
import itertools


def distancia(a, b):
	return math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)
	

class Problema(object):
	"""
	Representación genérica de un problema
	"""
	def __init__(self, genes, long_individuo):
		self.genes = genes[:]
		self.long_individuo = long_individuo
	def decodifica(self, x):
		pass
	def f_objetivo(self, x):
		pass


class OptimizacionCuadrado(Problema):
	"""
	Representación del problema de optimización de 2^n
	"""
	def __init__(self, n):
		Problema.__init__(self, [0, 1], n)
	def decodifica(self, x):
		return int(''.join(map(str, x)), 2)
	def f_objetivo(self, x):
		return 2**x


class NReinas(Problema):
	"""
	Representación del problema de las N-Reinas
	"""
	def __init__(self, n):
		Problema.__init__(self, range(1, n+1), n)
		self.max_jaques = (n*(n-1))/2
	def decodifica(self, x):
		return x.genes
	def f_objetivo(self, x):
		jaques = 0
		for i, i2 in enumerate(x):
			for j in xrange(i+1, len(x)):
				j2 = x[j]
				if i2 == j2 or abs(i-j) == abs(i2-j2):
					jaques += 1
		return self.max_jaques - jaques


class ProblemaViajante(Problema):
	"""
	Representación del problema del viajante (Travelling Salesman Problem)
	"""
	def __init__(self, ruta_fichero):
		self.mapa = self.importar_mapa(ruta_fichero)
		self.max_distancia = self.calcular_distancia_max()
		Problema.__init__(self, range(1, len(self.mapa)+1), len(self.mapa))
	def decodifica(self, x):
		return x.genes
	def f_objetivo(self, x):
		return self.max_distancia - self.distancia_recorrido(x)

	def calcular_distancia_max(self):
		combinations = itertools.combinations(self.mapa.values(), 2)
		return reduce(max, map(lambda (a,b): distancia(a, b), combinations)) * len(self.mapa)
	
	def distancia_recorrido(self, x):
		res = 0
		for i in xrange(len(x)-1):
			p1 = self.mapa[x[i]]
			p2 = self.mapa[x[i+1]]
			res += distancia(p1,p2)
		res += distancia(self.mapa[x[-1]], self.mapa[x[1]])
		return res
	
	def importar_mapa(self, ruta):
		mapa = {}
		with open(ruta) as f:
			for line in f.readlines():
				m = line.split(' ')
				if m[0].isdigit():
					mapa[int(m[0])] = (float(m[1]), float(m[2]))
		return mapa
