# -*- coding: utf-8 -*-

import unittest

from gavaps.algoritmos import GA, GAVaPS
from gavaps.problemas import NReinas


class TestAlgoritmos(unittest.TestCase):
    
    def setUp(self):
        self.problema = NReinas(4)
    
    def testGA(self):
        ga = GA(self.problema)
        _, _, valor = ga.algoritmo_genetico(50, 50, 0.7, 0.1)
        self.assertEqual(6, valor)

    def __testGAVaPS(self):
        ga = GAVaPS(self.problema)
        _, _, valor = ga.algoritmo_genetico(50, 50, 0.7, 0.1)
        self.assertEqual(6, valor)

    def tearDown(self):
        del self.problema
