# -*- coding: utf-8 -*-

import math
import unittest

from gavaps.individuo import Individuo
from gavaps.problemas import distancia
from gavaps.operadores import rango_aleatorio, inversion


class TestAuxiliares(unittest.TestCase):

    def testRangoAleatorio(self):
        pos1, pos2 = rango_aleatorio(0, 10)
        self.assertLessEqual(pos1, pos2)
    
    def testDistancia(self):
        p1 = (1,0)
        p2 = (2,1)
        self.assertEqual(math.sqrt(2), distancia(p1,p2))

    def testMutacion(self):
        individuo = Individuo([1,2,3,4,5,6,7,8,9])
        inversion(individuo, 2, 6)
        self.assertEqual([1,2,6,5,4,3,7,8,9], individuo.genes)
