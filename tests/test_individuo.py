# -*- coding: utf-8 -*-

import unittest

from gavaps.individuo import Individuo


class TestIndividuo(unittest.TestCase):
    
    def setUp(self):
        self.individuo = Individuo([])

    def test_incrementa_edad(self):
        self.individuo.edad = 5
        self.individuo.incrementa_edad()
        self.assertEqual(4, self.individuo.edad)
    
    def test_vivo(self):
        self.individuo.edad = 2
        self.individuo.incrementa_edad()
        self.assertTrue(self.individuo.vivo())
        
    def test_no_vivo(self):
        self.individuo.edad = 1
        self.individuo.incrementa_edad()
        self.assertFalse(self.individuo.vivo())

    def tearDown(self):
        del self.individuo
