# -*- coding: utf-8 -*-

import unittest

from gavaps.individuo import Individuo
from gavaps.operadores import ox, cx, pmx


class TestCrossovers(unittest.TestCase):

    def setUp(self):
        self.p1 = Individuo([1,2,3,4,5,6,7,8,9])
        self.p2 = Individuo([4,5,2,1,8,7,6,9,3])

    # Ejemplo en p.216 de [3]
    def testPMX(self):
        off1, off2 = pmx(self.p1, self.p2, 3, 7)
        self.assertEqual([4,2,3,1,8,7,6,5,9], off1.genes)
        self.assertEqual([1,8,2,4,5,6,7,9,3], off2.genes)
    
    # Ejemplo en p.217 de [3]
    def testOX(self):
        off1, off2 = ox(self.p1, self.p2, 3, 7)
        self.assertEqual([2,1,8,4,5,6,7,9,3], off1.genes)
        self.assertEqual([3,4,5,1,8,7,6,9,2], off2.genes)
    
    # Ejemplo en p.218 de [3]
    def testCX(self):
        self.p1 = Individuo([1,2,3,4,5,6,7,8,9])
        self.p2 = Individuo([4,1,2,8,7,6,9,3,5])
        off1, off2 = cx(self.p1, self.p2)
        self.assertEqual([1,2,3,4,7,6,9,8,5], off1.genes)
        self.assertEqual([4,1,2,8,5,6,7,3,9], off2.genes)
    
    def tearDown(self):
        del self.p1
        del self.p2
