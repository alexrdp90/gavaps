# -*- coding: utf-8 -*-

import time
import jinja2

from gavaps.algoritmos import GAVaPS, ProportionalAllocation
from gavaps.operadores import crossover_pmx, crossover_cx
from gavaps.problemas import ProblemaViajante


TEMPLATE = jinja2.Template("""<!DOCTYPE html><html>
<head><title>Tabla de resultados</title></head>
<body>
    <table>
        <tr>
            <th>Generacion</th>
            <th>Medio</th>
            <th>M&aacute;ximo</th>
        </tr>
        {% for statistic in statistics %}
        <tr>
            <td>{{ loop.index }}</td>
            <td>{{ statistic[2] }}</td>
            <td>{{ statistic[1] }}</td>
        </tr>
        {% endfor %}
    </table>
    <footer>Calculated in {{ time }} ms</footer>
</body></html>""")


def analiza_tsp(mapa, op_cruce, allocation, args):
	gavaps = GAVaPS(ProblemaViajante(mapa), allocation)
	gavaps.op_cruce = op_cruce
	start_time = time.time()
	apply(gavaps.algoritmo_genetico, args)
	total_time = time.time() - start_time
	print TEMPLATE.render(statistics=gavaps.estadisticas, time=total_time)


if __name__ == '__main__':
	analiza_tsp("mapas/QA194", crossover_pmx, ProportionalAllocation(), [50, 20, 0.35, 0.1])
	analiza_tsp("mapas/QA194", crossover_cx, ProportionalAllocation(), [50, 20, 0.35, 0.1])
